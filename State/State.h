#ifndef STATE_H
#define STATE_H

/*
*状态模式是一种行为设计模式， 让你能在一个对象的内部状态变化时改变其行为， 使其看上去就像改变了自身所属的类一样。这其实就有点类似算法中的有限状态机的形式。
*其主要思想是程序在任意时刻仅可处于几种有限的状态中。 在任何一个特定状态中， 程序的行为都不相同， 且可瞬间从一个状态切换到另一个状态。 不过， 根据当前
*状态，程序可能会切换到另外一种状态， 也可能会保持当前状态不变。 这些数量有限且预先定义的状态切换规则被称为转移。
*/

#include <iostream>

using namespace std;

// 状态类基类
class War;
class State
{
public:
    virtual void Prophase() {}
    virtual void Metaphase() {}
    virtual void Anaphase() {}
    virtual void End() {}
    virtual void CurrentState(War *war) {}
};
//战争
class War
{
private:
    State *m_state;  //目前状态
    int m_days;      //战争持续时间
public:
    War(State *state): m_state(state), m_days(0) {}
    ~War() {
        delete m_state;
    }
    int GetDays() {
        return m_days;
    }
    void SetDays(int days) {
        m_days = days;
    }
    void SetState(State *state) {
        delete m_state;
        m_state = state;
    }
    void GetState() {
        m_state->CurrentState(this);
    }
};

// 具体的状态类

//战争结束
class EndState: public State
{
public:
    void End(War *war) //结束阶段的具体行为
    {
        cout<<"战争结束"<<endl;
    }
    void CurrentState(War *war) {
        End(war);
    }
};

//后期
class AnaphaseState: public State
{
public:
    void Anaphase(War *war) //后期的具体行为
    {
        if(war->GetDays() < 30)
            cout<<"第"<<war->GetDays()<<"天：战争后期，双方拼死一搏"<<endl;
        else
        {
            war->SetState(new EndState());
            war->GetState();
        }
    }
    void CurrentState(War *war) {
        Anaphase(war);
    }
};

//中期
class MetaphaseState: public State
{
public:
    void Metaphase(War *war) //中期的具体行为
    {
        if(war->GetDays() < 20)
            cout<<"第"<<war->GetDays()<<"天：战争中期，进入相持阶段，双发各有损耗"<<endl;
        else
        {
            war->SetState(new AnaphaseState());
            war->GetState();
        }
    }
    void CurrentState(War *war) {
        Metaphase(war);
    }
};

//前期
class ProphaseState: public State
{
public:
    void Prophase(War *war)  //前期的具体行为
    {
        if(war->GetDays() < 10)
            cout<<"第"<<war->GetDays()<<"天：战争初期，双方你来我往，互相试探对方"<<endl;
        else
        {
            war->SetState(new MetaphaseState());
            war->GetState();
        }
    }
    void CurrentState(War *war) { Prophase(war); }
};

//测试案例
void StateTest()
{
    cout <<"\n===== State ====="<< endl;
    War *war = new War(new ProphaseState());
    for(int i = 1; i < 40;i += 5)
    {
        war->SetDays(i);
        war->GetState();
    }
    delete war;
}

#endif // STATE_H
