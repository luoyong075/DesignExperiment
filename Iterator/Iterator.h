#ifndef ITERATOR_H
#define ITERATOR_H

/*
* Iterator（迭代器）
* 迭代器模式是一种行为设计模式， 它可以有效管理数据流动的同时，让用户能在不暴露集合底层表现形式 （列表、 栈和树等） 的情况下遍历集合中所有的元素。
* 迭代器通常会提供一个获取集合元素的基本方法。 客户端可不断调用该方法直至它不返回任何内容， 这意味着迭代器已经遍历了所有元素。
* 在迭代器中主要分为两大主要部分：
* 1.容器部分，内部可以存放一系列数据，并提供迭代器调用接口。
* 2.迭代器部分，内部含有对容器部分的调用，并获得容器内部提供的方法。
* https://hermit.blog.csdn.net/article/details/123429647
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

/**
* @brief 简单概述
* @brief 迭代器基类
*/
class Iterator
{
public:
    Iterator(){};
    virtual ~Iterator(){};
    virtual string First() = 0;
    virtual string Next() = 0;
    virtual string GetCur() = 0;
    virtual bool IsEnd() = 0;
};
/**
* @brief 简单概述
* @brief 对象基类
*/
class Aggregate
{
public:
    Aggregate(){};
    virtual ~Aggregate(){};
    virtual int Count() = 0;
    virtual void Push(const string &strValue) = 0;
    virtual string Pop(const int index) = 0;
    virtual Iterator *CreateIterator() = 0;
};
/**
* @brief 简单概述
* @brief 迭代器子类
*/
class ConcreteIterator : public Iterator
{
public:
    ConcreteIterator(Aggregate *pAggregate) : Iterator()
    {
        m_nCurrent = 0;
        m_Aggregate = pAggregate;
    }

    string First()
    {
        return m_Aggregate->Pop(0);
    }

    string Next()
    {
        string strRet;
        m_nCurrent++;

        if(m_nCurrent < m_Aggregate->Count())
        {
            strRet = m_Aggregate->Pop(m_nCurrent);
        }

        return strRet;
    }

    string GetCur()
    {
        return m_Aggregate->Pop(m_nCurrent);
    }

    bool IsEnd()
    {
        return ((m_nCurrent >= m_Aggregate->Count()) ? true : false);
    }

private:
    int m_nCurrent;
    Aggregate *m_Aggregate;
};
/**
* @brief 简单概述
* @brief 对象类
*/
class ConcreteAggregate : public Aggregate
{
public:
    ConcreteAggregate() : m_pIterator(NULL)
    {
        m_vecItems.clear();
    }

    ~ConcreteAggregate()
    {
        if(m_pIterator != NULL)
        {
            delete m_pIterator;
            m_pIterator = NULL;
        }
    }

    Iterator *CreateIterator()
    {
        if(m_pIterator == NULL)
        {
            m_pIterator = new ConcreteIterator(this);
        }

        return m_pIterator;
    }

    int Count()
    {
        return m_vecItems.size();
    }

    void Push(const string &strValue)
    {
        m_vecItems.push_back(strValue);
    }

    string Pop(const int index)
    {
        string strRet;

        if(index < Count())
        {
            strRet = m_vecItems[index];
        }

        return strRet;
    }

private:
    Iterator *m_pIterator;
    vector<string> m_vecItems;
};

//测试
void IteratorTest()
{
    cout <<"\n===== Iterator ====="<< endl;
    ConcreteAggregate *pName = new ConcreteAggregate();
    if(pName == NULL)
        return;

    pName->Push("hello");
    pName->Push("world");
    pName->Push("cxue");

    Iterator *iter = NULL;
    iter = pName->CreateIterator();

    if(iter != NULL)
    {
        string strItem = iter->First();

        while(!iter->IsEnd())
        {
            cout << iter->GetCur() << " is ok" << endl;
            iter->Next();
        }
    }

    delete pName;
}

#endif // ITERATOR_H
