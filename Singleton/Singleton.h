#ifndef SINGLETON_H
#define SINGLETON_H

#include <iostream>
#include <mutex>
/*
* 单例模式(创建型)顾名思义，保证一个类仅可以有一个实例化对象，并且提供一个可以访问它的全局接口。实现单例模式必须注意一下几点：
* 1.单例类只能由一个实例化对象。
* 2.单例类必须自己提供一个实例化对象。
* 3.单例类必须提供一个可以访问唯一实例化对象的接口。
* 单例模式分为懒汉和饿汉两种实现方式。
*/
using namespace std;

//线程安全的懒汉单例模式
std::mutex mt;

class Singleton
{
public:
    static Singleton* getInstance();
    void print(){
        cout <<"Singleton print"<< endl;
    }
private:
    Singleton(){}                                    //构造函数私有
    Singleton(const Singleton&) = delete;            //明确拒绝
    Singleton& operator=(const Singleton&) = delete; //明确拒绝

    static Singleton* m_pSingleton;

};
Singleton* Singleton::m_pSingleton = NULL;

Singleton* Singleton::getInstance()
{
    if(m_pSingleton == NULL)
    {
        mt.lock();
        if(m_pSingleton == NULL)
        {
            cout <<"Singleton run construct"<< endl;
            m_pSingleton = new Singleton();
        }
        mt.unlock();
    }
    else
        cout <<"Singleton already construct"<< endl;
    return m_pSingleton;
}

void SingletonTest(){
    cout <<"=== Singleton ===="<< endl;
    Singleton* sg=Singleton::getInstance();
    sg->print();
    Singleton* sg2=Singleton::getInstance();
    sg2->print();
}

#endif // SINGLETON_H
