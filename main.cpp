#include <QCoreApplication>
#include <string>
#include <iostream>
#include "StringExt.h"
#include "Factory/SimpleFactory.h"
#include "Factory/MethodFactory.h"
#include "Factory/AbstractFactory.h"
#include "Strategy/StrategyOld.h"
#include "Singleton/Singleton.h"
#include "Proxy/Proxy.h"
#include "Adapter/Adapter.h"
#include "Composite/Composite.h"
#include "Prototype/Prototype.h"
#include "Builder/Builder.h"
#include "Flyweight/Flyweight.h"
#include "Bridge/Bridge.h"
#include "Visitor/Visitor.h"
#include "Filter/Filter.h"
#include "Observer/Observer.h"
#include "Command/Command.h"
#include "State/State.h"
#include "Iterator/Iterator.h"
#include "Interpreter/Interpreter.h"
#include "ChainResponsibility/ChainResponsibility.h"
#include "Decorator/Decorator.h"
#include "Facade/Facade.h"
#include "Mediator/Mediator.h"
#include "Memento/Memento.h"
#include "Template/Template.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    std::u16string u16str=to_utf16("abc");

    AdapterTest();

    BridgeTest();

    OrderBuilderTest();

    ChainResponsibilityTest();

    CommandTest();

    CompositeTest();

    DecoratorTest();

    FacadeTest();

    SimpleFactoryTest();

    MethodFactoryTest();

    AbstractFactoryTest();

    FilterTest();

    FlyweightTest();

    InterpreterTest();

    IteratorTest();

    MediatorTest();

    MementoTest();

    ObserverTest();

    ProtoTypeTest();

    ProxyTest();

    SingletonTest();

    StateTest();

    StrategyOldTest();

    Design::TemplateTest();

    VisitorTest();

    return a.exec();
}
