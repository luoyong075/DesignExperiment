#ifndef SIMPLEFACTORY_H
#define SIMPLEFACTORY_H

#include <string>
#include <iostream>
//简单工厂模式
//主要特点是需要在工厂类中做判断，从而创造相应的产品，当增加新产品时，需要修改工厂类。使用简单工厂模式，我们只需要知道具体的产品型号就可以创建一个产品。
//缺点：工厂类集中了所有产品类的创建逻辑，如果产品量较大，会使得工厂类变的非常臃肿。
using namespace std;
//define factory type
typedef enum
{
    Tank_Type_59,
    Tank_Type_96,
    Tank_Type_99,
    Tank_Type_Num
}Tank_Type;

//抽象产品类
class Tank
{
public:
   virtual const string& type() = 0;
   virtual void fire() =0;
};

//tank 59 product
class Tank59 : public Tank
{
public:
    Tank59():Tank(),m_strType("Tank59")
    {
        cout <<"Tank56 create"<< endl;
    }
    const string& type() override
    {
        cout << m_strType.data() <<" type"<< endl;
        return m_strType;
    }

    void fire() override{
        cout << m_strType.data() << " fire"<< endl;
    }

private:
    string m_strType;
};

//tank 96 product
class Tank96:public Tank
{
public:
    Tank96():Tank(),m_strType("Tank96")
    {
        cout <<"Tank96 create"<< endl;
    }
    const string& type() override
    {
        cout << m_strType.data() <<" type"<< endl;
        return m_strType;
    }
    void fire() override{
        cout << m_strType.data() << " fire"<< endl;
    }
private:
    string m_strType;
};

//tank 99 product
class Tank99 : public Tank
{
public:
    Tank99():Tank(),m_strType("Tank99")
    {
        cout <<"Tank99 create"<< endl;
    }
    const string& type() override
    {
        cout << m_strType.data() << " type"<< endl;
        return m_strType;
    }
    void fire() override{
        cout << m_strType.data() << " fire"<< endl;
    }
private:
    string m_strType;
};

class SimpleFactory
{
public:
    SimpleFactory(){
        std::cout <<"SimpleFactory create"<< std::endl;
    }

    //根据产品信息创建具体的产品类实例，返回一个抽象产品类
    Tank* createTank(Tank_Type type)
    {
        switch(type)
        {
        case Tank_Type_59:
            return new Tank59();
        case Tank_Type_96:
            return new Tank96();
        case Tank_Type_99:
            return new Tank99();
        default:
            return nullptr;
        }
    }

};

void SimpleFactoryTest(){
    cout <<"=== Simple Factory ==="<< endl;
    SimpleFactory* factory = new SimpleFactory();
    Tank* tank59 = factory->createTank(Tank_Type_59);
    tank59->type();
    tank59->fire();
    Tank* tank96 = factory->createTank(Tank_Type_96);
    tank96->type();
    tank96->fire();

    delete tank96;
    tank96 = nullptr;

    delete tank59;
    tank59 = nullptr;

    delete factory;
    factory = nullptr;
}

#endif // SIMPLEFACTORY_H
