#ifndef METHODFACTORY_H
#define METHODFACTORY_H

#include <string>
#include <iostream>
#include "SimpleFactory.h"
//工厂方法模式(创建型)
//定义一个创建对象的接口，其子类去具体现实这个接口以完成具体的创建工作。如果需要增加新的产品类，只需要扩展一个相应的工厂类即可。
//缺点：产品类数据较多时，需要实现大量的工厂类，这无疑增加了代码量。
using namespace std;

//抽象工厂类，提供一个创建接口
class TankFactory
{
public:
   //提供创建产品实例的接口，返回抽象产品类
   virtual Tank* createTank() = 0;
};

//具体的创建工厂类，使用抽象工厂类提供的接口，去创建具体的产品实例
class Tank59Factory : public TankFactory
{
public:
    Tank* createTank() override
    {
        return new Tank59();
    }
};

class Tank96Factory : public TankFactory
{
public:
    Tank* createTank() override
    {
        return new Tank96();
    }
};

class Tank99Factory : public TankFactory
{
public:
    Tank* createTank() override
    {
        return new Tank99();
    }
};

void MethodFactoryTest(){
    cout <<"=== Method Factory ==="<< endl;
    TankFactory* factory59 = new Tank59Factory();
    Tank* tank59 = factory59->createTank();
    tank59->type();
    tank59->fire();

    TankFactory* factory96 = new Tank96Factory();
    Tank* tank96 = factory96->createTank();
    tank96->type();
    tank96->fire();

    delete tank96;
    tank96 = nullptr;
    delete factory96;
    factory96 = nullptr;

    delete tank59;
    tank59 = nullptr;
    delete factory59;
    factory59 = nullptr;
}

#endif // METHODFACTORY_H
