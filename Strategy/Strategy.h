#ifndef METHODFACTORY_H
#define METHODFACTORY_H

#include <iostream>
#include <functional>
/*
* 策略模式(行为型模式)是指定义一系列的算法，把它们单独封装起来，并且使它们可以互相替换，使得算法可以独立于使用它的客户端而变化，也是说这些算法所完成的功能类型是一样的，对外接口也是一样的，只是
* 不同的策略为引起环境角色环境角色表现出不同的行为。
* 相比于使用大量的if...else，使用策略模式可以降低复杂度，使得代码更容易维护。
* 缺点：可能需要定义大量的策略类，并且这些策略类都要提供给客户端。
* [环境角色]  持有一个策略类的引用，最终给客户端调用。
*/
using namespace std;

//使用函数指针实现策略模式
​
void adcHurt()
{
    std::cout << "Adc Hurt" << std::endl;
}
​
void apcHurt()
{
    std::cout << "Apc Hurt" << std::endl;
}
​
//环境角色类， 使用传统的函数指针
class Soldier
{
public:
    typedef void (*Function)();
    Soldier(Function fun): m_fun(fun)
    {
    }
    void attack()
    {
        m_fun();
    }
private:
    Function m_fun;
};
​
//环境角色类， 使用std::function<>
class Mage
{
public:
    typedef std::function<void()> Function;
​
    Mage(Function fun): m_fun(fun)
    {
    }
    void attack()
    {
        m_fun();
    }
private:
    Function m_fun;
};

void StrategyTest(){
{
    Soldier* soldier = new Soldier(apcHurt);
    soldier->attack();
    delete soldier;
    soldier = nullptr;
}

#endif // METHODFACTORY_H
