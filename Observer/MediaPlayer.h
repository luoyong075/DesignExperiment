#ifndef MEDIAPLAYER_H
#define MEDIAPLAYER_H

/*
 * 简化版的观察者模式：mediaPlayer。
 *
*/

#include <iostream>
#include <list>
#include <memory>
#include <algorithm>

using namespace std;

class MediaPlayerInterface
{
public:
    virtual ~MediaPlayerInterface(){}
    virtual int OnMessage(int message)=0;
};

class MediaPlayer
{
public:
    MediaPlayer(MediaPlayerInterface *pMediaPlayerInterface){
        m_mediaPlayerInterface=pMediaPlayerInterface;
    }
    virtual ~MediaPlayer(){}

    void setSourcePath(string path){
        cout << "setSourcePath:" << path<<endl;
    }

    void prepareSync(){
        cout << "video prepareSync:" << endl;
    }

    void start(){
        cout << "video start:" << endl;
    }

    void seek(int pos){
        cout << "video seek:" << pos<<endl;
    }

    void stop(){
        cout << "video stop:" << endl;
    }
private:
    MediaPlayerInterface*  m_mediaPlayerInterface=nullptr;
};

int ObserverTest2()
{
    return 0;
}

#endif // MEDIAPLAYER_H
