#ifndef COMMAND_H
#define COMMAND_H

/*
*Command(命令模式):命令模式就是一种行为设计模式，它可将请求转换为一个包含与请求相关的所有信息的独立对象。并能根据不同的请求将方法参数化、延迟请求执行或将其放入队列中，且能实现可撤销操作
*我们在遇到类似GUI中多种Button点击功能的操作时，我们应该将请求的所有细节 （例如调用的对象、 方法名称和参数列表） 抽取出来组成命令类， 该类中仅包含一个用于触发请求的方法。
*/

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>

using namespace std;

// 接受者，作为最底层的调用
class Receiver
{
public:
        void BakeMutton()
        {
                cout<< "bake mutton"<< endl;
        }

        void BakeChicken()
        {
                cout<< "bake chicken"<< endl;
        }
};

// 基类
class Command
{
public:
        Command(Receiver* pstReceiver):m_pstReceiver(pstReceiver)
        {

        }
        virtual void Excute() = 0;

protected:
        Receiver* m_pstReceiver;
};

// 具体类，用于调用接收者
class ConcreteCommandA: public Command
{
public:
        ConcreteCommandA(Receiver* pstReceiver):Command(pstReceiver)
        {

        }
        virtual void Excute()
        {
                cout<< "ConcreteCommandA excuting......"<< endl;
                m_pstReceiver->BakeMutton();
        }

};

// 具体类，用于调用接收者
class ConcreteCommandB: public Command
{
public:
        ConcreteCommandB(Receiver* pstReceiver):Command(pstReceiver)
        {

        }
        virtual void Excute()
        {
                cout<< "ConcreteCommandB excuting......"<< endl;
                m_pstReceiver->BakeChicken();
        }
};

// 调用者，作为最上层，用于管理具体类的操作，从而对接收者增删
class Invoke
{
public:
        void Add(Command* pstCommand)
        {
                m_vecPstCommand.push_back(pstCommand);
        }
        void Remove(Command* pstCommand)
        {
                m_vecPstCommand.erase(find(m_vecPstCommand.begin(), m_vecPstCommand.end(), pstCommand));
        }
        void RemoveAll()
        {
                m_vecPstCommand.clear();
        }
        void Notify()
        {
                for (typeof(m_vecPstCommand.begin()) it = m_vecPstCommand.begin(); it != m_vecPstCommand.end(); ++it)
                {
                        (*it)->Excute();
                }
        }

private:
        vector<Command*> m_vecPstCommand;
};

void CommandTest()
{
    std::cout <<"\n===== Command ====="<< std::endl;
    Receiver* pstReceiver = new Receiver();
    Command* pstConcreteCommandA = new ConcreteCommandA(pstReceiver);
    Command* pstConcreteCommandB = new ConcreteCommandB(pstReceiver);
    Invoke* pstInvoke = new Invoke();

    pstInvoke->Add(pstConcreteCommandA);
    pstInvoke->Add(pstConcreteCommandA);
    pstInvoke->Add(pstConcreteCommandB);
    pstInvoke->Notify();
    cout<< "------------------"<< endl<< endl;

    pstInvoke->Remove(pstConcreteCommandA);  //撤销操作
    pstInvoke->Remove(pstConcreteCommandB);
    pstInvoke->Notify();
    cout<< "------------------"<< endl<< endl;
}

#endif // COMMAND_H
